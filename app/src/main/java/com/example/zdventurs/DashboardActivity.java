package com.example.zdventurs;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.example.zdventurs.adapters.BookingAdapter;
import com.example.zdventurs.helpers.AuthHelper;
import com.example.zdventurs.helpers.BookingHelper;
import com.example.zdventurs.models.Booking;
import com.example.zdventurs.models.User;

import java.util.ArrayList;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class DashboardActivity extends AppCompatActivity {
    AuthHelper authHelper;
    ImageButton signout;
    FloatingActionButton create;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        authHelper = new AuthHelper(DashboardActivity.this);
        signout = (ImageButton) findViewById(R.id.signout);
        create = (FloatingActionButton) findViewById(R.id.create);

        loadUser();
        loadList(true);

        signout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                authHelper.attemptlogout();
                Intent intent = new Intent(DashboardActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DashboardActivity.this, BookingActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadList(false);
    }

    public String initial(String str, int n) {
        if (!str.isEmpty()) {
            String[] strs = str.split(" ");
            str = "";
            for (int i = 0; i < Math.min(n, strs.length); i++) {
                str = str.concat(strs[i].substring(0, 1));
            }
            return str;
        }
        return "";
    }

    public void loadList(boolean isCreate) {
        AuthHelper authHelper = new AuthHelper(DashboardActivity.this);
        BookingHelper bookingHelper = new BookingHelper(DashboardActivity.this);
        User user = authHelper.details();
        final ArrayList<Booking> bookings = bookingHelper.getAll(String.valueOf(user.getId()));
        BookingAdapter bookingAdapter = new BookingAdapter(DashboardActivity.this, bookings);
        ListView listBooking = (ListView) findViewById(R.id.list_booking);
        View listItemView = LayoutInflater.from(DashboardActivity.this).inflate(
                R.layout.list_item_footer, null);
        if (isCreate) {
            listBooking.addFooterView(listItemView, null, false);
        }
        listBooking.setAdapter(bookingAdapter);
    }

    public void loadUser() {
        User user = authHelper.details();
        TextView initial = (TextView) findViewById(R.id.initial);
        TextView name = (TextView) findViewById(R.id.name);
        if (user != null) {
            initial.setText(initial(user.getName(), 2));
            name.setText(user.getName());
        }
    }


}