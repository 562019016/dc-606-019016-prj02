package com.example.zdventurs;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.example.zdventurs.helpers.AuthHelper;

public class LoginActivity extends AppCompatActivity {
    Button btnSignin;
    Button btnSignup;
    EditText edtUsername;
    EditText edtPassword;
    AuthHelper authHelper;
    FrameLayout frLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btnSignin = (Button) findViewById(R.id.signin);
        btnSignup = (Button) findViewById(R.id.signup);
        edtUsername = (EditText) findViewById(R.id.username);
        edtPassword = (EditText) findViewById(R.id.password);
        frLoading = (FrameLayout) findViewById(R.id.loading);
        authHelper = new AuthHelper(LoginActivity.this);

        btnSignin.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                String username = edtUsername.getText().toString();
                String password = edtPassword.getText().toString();
                Handler handler = new Handler();

                frLoading.setVisibility(View.VISIBLE);
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(authHelper.attemptLogin(username, password)) {
                            Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        } else {
                            frLoading.setVisibility(View.GONE);
                            Toast.makeText(getBaseContext(), "Data login tidak valid!", Toast.LENGTH_LONG).show();
                        }
                    }
                }, 1000);
            }
        });

        btnSignup.setOnClickListener(new View.OnClickListener(){
            @Override
            public  void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
        });
    }
}