package com.example.zdventurs;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.zdventurs.models.User;
import com.google.android.material.textfield.TextInputLayout;

import android.widget.FrameLayout;
import android.widget.Toast;

import com.example.zdventurs.helpers.AuthHelper;
import com.example.zdventurs.models.User;

public class RegisterActivity extends AppCompatActivity {
    Button btnSignup;
    EditText edtName;
    EditText edtUsername;
    EditText edtPassword;
    EditText edtRepassword;
    TextInputLayout txUsername;
    TextInputLayout txRepassword;
    AuthHelper authHelper;
    FrameLayout frLoading;
    User user;
    int valid;
    int reqValid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getSupportActionBar().setTitle("Register");

        btnSignup = (Button) findViewById(R.id.signup);
        edtName = (EditText) findViewById(R.id.name);
        edtUsername = (EditText) findViewById(R.id.username);
        edtPassword = (EditText) findViewById(R.id.password);
        edtRepassword = (EditText) findViewById(R.id.repassword);
        txUsername = (TextInputLayout) findViewById(R.id.username_field);
        txRepassword = (TextInputLayout) findViewById(R.id.repassword_field);
        frLoading = (FrameLayout) findViewById(R.id.loading);
        authHelper = new AuthHelper(RegisterActivity.this);
        valid = 0;
        reqValid = 15;

        edtName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edtName.getText().length() > 0)
                    valid |= 8;
                else
                    valid &= reqValid ^ 8;
                btnSignup.setEnabled(valid == reqValid);
            }
        });

        edtUsername.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String username = edtUsername.getText().toString();
                // validation value
                if (!username.isEmpty()) {
                    valid |= 4;
                    txUsername.setError(null);
                    txUsername.setErrorEnabled(false);
                }
                if (authHelper.checkUsername(username)) {
                    valid &= reqValid ^ 4;
                    txUsername.setError("Username sudah digunakan!");
                    txUsername.setErrorEnabled(true);
                }
                btnSignup.setEnabled(valid == reqValid);
            }
        });

        edtPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edtPassword.getText().length() > 0)
                    valid |= 2;
                else
                    valid &= reqValid ^ 2;
                btnSignup.setEnabled(valid == reqValid);
            }
        });

        edtRepassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String password = edtPassword.getText().toString();
                String repassword = edtRepassword.getText().toString();
                if (password.equals(repassword)) {
                    valid |= 1;
                    txRepassword.setErrorEnabled(false);
                    txRepassword.setError(null);
                } else {
                    valid &= reqValid ^ 1;
                    txRepassword.setErrorEnabled(true);
                    txRepassword.setError("Kata sandi tidak sama!");
                }
                btnSignup.setEnabled(valid == reqValid);
            }
        });

        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = edtName.getText().toString();
                String username = edtUsername.getText().toString();
                String password = edtPassword.getText().toString();
                Handler handler = new Handler();
                user = new User(0, name, username, password);
                frLoading.setVisibility(View.VISIBLE);

                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (authHelper.create(user)) {
                            Intent intent = new Intent(RegisterActivity.this, DashboardActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        } else {
                            frLoading.setVisibility(View.GONE);
                            Toast.makeText(getBaseContext(), "Data pendaftaran tidak valid", Toast.LENGTH_LONG).show();
                        }
                    }
                }, 200);
            }
        });

    }
}